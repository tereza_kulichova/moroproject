package com.greglturnquist.payroll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by TEREZA on 17.06.17.
 */

@RestController
@RequestMapping (value = "zamestnanec")
public class EmployeeController {

    @Autowired
    private EmployeeRepository repository;

    @RequestMapping (method = RequestMethod.GET)
    public Iterable<Employee> seznamZamestnancu() {
        return repository.findAll();

    }
}
